# de kinderen stellen de vragen

## 1

we zoeken een gebouw

## 2

Opnieuw een gebouw

## 3

We vroegen de kinderen ook een aantal begrippen uit te leggen.
Waar hebben ze het hier over?

## 4

Welke begrip leggen de kinderen hier met hand en tand uit?

## 5

Kinderen hebben ook hun eigen taal, ook al begrijpen ze die zelf soms niet.

## 6

Toen hebben we de koters een koptelefoon opgezet en ze gevraagd om de liedjes die ze hoorden voor ons te zingen.
We zoeken telkens titel en uitvoerder. En met uitvoerder bedoelen we dus niet de koters.

## 7

Titel en uitvoerder van het volgende liedje

## 8

De grotere kinderen kregen de tekst te zien samen met de muziek.

## 9

Zelfde vraag: titel en uitvoerder

## 10

En de laatste