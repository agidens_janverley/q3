# Ronde 7

Deze ronde gaat over de kwis zelf, dus als u vanavond goed heeft opgelet, wordt dit een makkie.

## 1

Voor de pauze had ik een andere trui aan.
Welke kleur had die trui?

## 2

Een van de topdiensters, Heidi, heeft daarnet een ronde lang opgediend met iets op haar hoofd.
Wat was dat?

## 3

Daarnet tijdens de pauze heb ik allemaal nummers van 1 welbepaalde groep gespeeld.
Welke groep?

## 4

Om iets na 8 zijn we begonnen aan deze kwis.

Wat waren mijn eerste woorden van deze kwis?
Voor de opletters: ik was niet met mijn telefoon aan het spelen, maar ik heb ondertussen opgenomen wat ik zei als bewijs materiaal voor zo dadelijk.

## 5

DE jury hier naast mij zit al heel de avond naarstig te werken.
Ik heb ze in het begin van de kwis voorgesteld. Maar een persoon van diegenen die ik toen voorgesteld heb, zit er nu niet bij.

Wie missen we op dit moment aan de jury tafel?

## 6

DING

Jullie weten nog wat het juiste antwoord was op de allereerste vraag van deze kwis,
maar weten jullie nog wat het juiste antwoord was op de tweede vraag van de eerste ronde?

DING

## 7

Buiten aan de inkomdeur hing een affiche voor het Schoolfeest. Op welke datum valt dat dit jaar?

## 8

Tijdens deze kwis waren er 3 vragen waarop het juiste antwoord hetzelfde was.
Was was het juiste antwoord op 3 vragen tot nu toe, dus met deze vraag erbij 4.

## 9

Bij de eerste ronde kregen jullie 10 filmpjes te zien met schattige kinderen die vanalles uitlegden en zongen.

Hoeveel kinderen deden er in totaal mee?

## 10

Welke ploeg stond op de eerste plaats toen we daarnet de laatste keer de tussenstand lieten zien?