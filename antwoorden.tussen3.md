# Puzzel ronde

## 1

Wie is het - Bill

## 2

Wie is het - George

## 3

Wie is het - Claire

## 4

Winkel - Switch

## 5

Winkel - HEMA

## 6

Logo - San Pellerino

## 7 

Logo - Coolblue

## 8

Logo - Panzani

## 9

Logo - Spotify

## 10

Juffen I - Kim, Johan, Peter

## 11

Juffen II - Sarah, Pascalle, Ilse

## 12

Droedel - Proper

## 13

Droedel - Advies

## 14

Cijferslot - 2

## 15

Luc Deflo

## 16

Elke Vanelderen

## 17

Jelle Cleymans

## 18

Je kunt dit probleem het handigst oplossen door niet te kijken wat er allemaal tussendoor gebeurt, 
maar alleen de begin en de eindsituatie te bekijken.
Omdat er evenveel vloeistof (een lepel) heen en weer gaat, weten we zeker dat in de eindsituatie de glazen weer even vol zijn.
Stel dat er op het eind een hoeveelheid H water bij de wijn zit. 
Omdat het wijnglas even vol is aan het begin, en er H water is bijgekomen, moet er wel H wijn verdwenen zijn.
Die wijn zit allemaal in het andere glas!
Dus er zit ook H wijn bij het water.

oplossing: even veel!

## 19 

Op het moment dat ik binnenkom zijn er maar twee mogelijkheden: 
•	de lege stoel is mijn eigen stoel
•	de lege stoel is de stoel van de eerste passagier

Tijdens het hele proces is er geen voorkeur voor één van deze beide situaties geweest. 
Er is geen verschil tussen het kiezen van mijn stoel of de stoel van de eerste passagier. Door niemand.

De kans op beide situaties is daarom gelijk, dus 50%



