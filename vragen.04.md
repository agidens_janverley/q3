# Ronde 4 De decenniën

We zoeken telkens 1 decennium in deze ronde

Voor de duidelijkheid: als het antwoord 1990 is, zet je een kruisje in de kolom "90", want 1990 valt in de jaren "90".
1999 ook, 
2000 valt in het decennium dat we met "00" aangeduid hebben.

## 1

Wanneer kwam het legendarische computer spel DOOM uit?
Het was een van de eerste en gigantisch populaire First Person Shooters, draaide op MS-dOS en werd uitgebracht door id Software.

## 2

De reeks "You Rang, M'Lord?" liep 4 seizoenen op de BBC. In welk decennium werd de eerste aflevering van het eerste seizoen uitgezonden?
Er werd voordien ook nog een pilot uitgezonden, maar die interesseert ons niet.

## 3 DING

Marijn Devalk werd in 1992 snor van het jaar, en hij speelt onder andere Boma in FC De kampioenen.
In welk van de decenniën werd de eerste aflevering van F.C. De Kampioenen uitgezonden?

## 4 DING

IN welk decennium moest Leona Detiege aftreden als burgemeester van Antwerpen naar aanleiding van de VISA affaire?

## 5

Ariel de kleine zeemeermin, de tekenfilm gebaseerd op het sprookje van Hans Christian Andersen, was de eerste animatiefilm waarmee Disney na lange tijd terug commercieel succes boekte.
In welk decennium kwam hij uit?

## 6

Een ander film die we zoeken. Iedereen weet dat Terminator 2 uitkwam in 1991,
maar in welk decennium kwam Terminator 3 uit?

## 7

We hebben een site gevonden waar het moment van eerste gebruik van bepaalde termen te vinden is: 
Merriam-Webster
op hun site staat , "America's most trusted online dictionary for English word definitions, meanings, and pronunciation." 
Een beetje de amerikaanse Van Dale dus.

De eerste term is "smartphone".
In welk decennium werd dit begrip voor het eerst gebruikt volgens Merriam-Webster.

## 8

Een andere term is "bucket list"

In welk decennium werd dit begrip voor het eerst gebruikt volgens Merriam-Webster.

## 9

Instagram valt niet meer weg te denken uit de leefwereld van hipsters en/of iedereen die tijd te veel heeft.

Maar in welk decennium werd deze App gelanceerd?

## 10

"U can't touch this"

simpel: in welk decennium werd dit nummer uitgebracht?

