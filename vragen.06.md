# ronde 6 ABC

In deze ronde verwachten we 26 antwoorden.
Die antwoorden beginnen dus allemaal met een andere letter, zoveel is duidelijk.

## 1

We zoeken de nederlandse benamingen van de hoofdsteden van de provincies van België.
Let op: Als meerdere steden met dezelfde letter zouden beginnen, verwachten we wel _alle_ steden te zien bij die letter.
Dus onzinnig antwoord: als Oostende en Oostmalle provincie hoofdsteden zouden zijn, dan vul je bij O die 2 namen in.

## 2

De man links op de foto heeft _dochters_. Ik geef je alvast de namen van 2: *Billie* en *Dorien*, Hoe heten de anderen?
Ook bij deze vraag mogen jullie dus bij meerdere letters iets schrijven, als hij meerdere dochters heeft.

## 3

We zoeken 1 term die verwijst naar "Technologie, Wetenschap, Wiskunde en Bouwkunde" via een actieplan tracht de Vlaamse overheid de jeugd meer te interesseren voor wetenschap en techniek; het letterwoord wordt gebruikt in tal van samenstellingen.

## 4

De Star-Wars vraag!
Hoe heet dit personage uit Episode I – The Phantom Menace uit 1999. Gespeeld door Liam Neeson. Het is een Jedi Master en mentor van Obi-Wan Kenobi, gespeeld in die episode door Ewan McGregor.

## 5

We zoeken een amerikaans bedrijf dat een netwerk van satelieten in de lucht houdt voor wereldwijde voice en data communicatie met handheld telefoons en andere apparaten.
Met op dit moment 75 satelieten in de lucht is dit netwerk uniek omdat het op elke plek op aarde bereikbaar is.
De laatste reeks satelieten werd begin dit jaar nog door SpaceX van Elon Musk de ruimte ingeschoten.

Hoe heet dit bedrijf?

## 6

We zoeken het chromosoom dat in diploïde cellen van individuen van het ene geslacht in tweevoud voorkomt en in diploïde cellen van het andere geslacht in enkelvoud.

## 7

We zoeken de Amerikaanse versie van wat bij ons jeugdherberg heet.
Daar is de organisatie wel christelijk geïnspireerd en is het officieel een "oecumenisch-christelijke jongerenorganisatie"
"Het is er leuk toeven" zeggen ze er daar over.

## 8

We zoeken een Amerikaanse rock band in 1969 in Houston, Texas opgericht. De leden zijn Billy Gibbons, Dusty Hill, en drummer Frank Beard. Hun plaat Eliminator uit 1983 verkocht het best, met deze fantastische Ford op de cover.

## 9

Een televisiequiz en taalspel tussen een Vlaams en een Nederlands team, sinds 1989 uitgezonden door verschillende omroepen in Nederland, en de VRT. Sinds 2006 was het programma alleen nog bij de KRO te zien en sinds 2009 spelen zelfs alleen nog maar Nederlanders mee. Gepresenteerd door Marcel Van Tilt en Anita Witzier.

## 10

Op het internet wordt verschrikkelijk hard gescholden en slecht geargumenteerd. Een redenering die je vaak ziet terug komen is dat iemand een Nazi genoemd wordt omdat hij of zij een mening deelt met Hitler: "Hitler was tegen roken, jij bent tegen roken, dus jij bent een Nazi". Wat is de pseudo latijnse benaming van deze redenering? De term die we zoeken bestaan al lang voor het internet trouwens, al sinds 1953.

## 11

De chemie vraag: de Tabel van Mendeljev was recent nog in het nieuws want hij is net 150 jaar oud geworden.
Proficiat Tabel!
Wat is het symbool voor het chemisch element Fosfor?

## 12

Opgelet we zoeken de _voornaam_ van de volgende persoon.
Het spel is begonnen vorige zondag.
Weet u nog wie in het eerste seizoen 1998-1999 De Mol was?

## 13

We zoeken het Friese woord voor "vuilnisemmer"
De term werd ook gebruik als naam van een humoristisch tv programma dat 15 jaar liep op de nederlands tv, met onder andere Herman Koch, Michiel Romeyn en Kees Prins.

## 14

België heeft een opvolger gekozen voor de F-16 vliegtuigen.
Voor welk toestel is er gekozen?

jullie zullen wel vermoeden bij welke letter WE het antwoord verwachten, maar welk getal hoorde daar bij?

## 15

DING

Naar wie was het huidige Bevrijdingsdok genoemd?

## 16

We zoeken de naam van een Nederlands Film uit 1984.
Het verhaal gaat over de familie Vrijmoeth en speelt zich af in de jaren 30.  
Een heel dramatisch script met verschrikkelijke gebeurtenissen maken het 1 van de triestigste verhalen ever.

Bekende koppen waren Willeke van Ammelrooy en Herman Van Veen, die ook die verdomde soundtrack schreef.

## 17

Aardrijkskunde: op 1 januari 2019 annexeerde Aalter een kleinere buurgemeente.
De lokale machthebbers, waaronder Pieter De Crem, verbloemden de zaak nog met holle slogans als "samensterkerbeter" en "Totaalter" 
maar lachten ondertussen hun nieuwe onderdanen uit door hun straten te hernoemen:
de "Schoolstraat" werd bijvoorbeeld "de Pierlalaweg".
Hoe heet de gemeente die begin dit jaar fuseerde met Aalter?

## 18

Nog aardrijkskunde: We zoeken een Amerikaanse staat die bekend staat om zijn Salt Lakes en geweldige Monument Valley. 
De hoofdstad is Salt Lake City, en er is geen Beach.

## 19

Vorige vrijdag 8 maart was er een internationale feestdag. Wat werd over heel de wereld gevierd?
