# Introductie

*REC*

Goeienavond Beste Kwissers en Kwissters

*STOP REC*

Wij zijn zeer blij dat jullie hier vanavond allemaal opgedaagd zijn voor de derde Vliegertje Kwis.
We zitten iets ruimer dan in de containers van vorig jaar. 
Hopelijk kan iedereen mij horen en de schermen zien?

De opdracht is duidelijk: wij stellen vragen, jullie beantwoorden die, 
jullie drinken en eten ondertussen zo veel mogelijk, en gaan op het einde met een fijn gevoel naar huis.

## Regels

Eerst overloop ik even de praktische zaken en regels:

- De kwis bestaat uit 2 delen van telkens 4 ronden met een pause tussenin. Tussen de gewonen vragen doormoeten jullie ook 3 tussenronden oplossen.
- Heeft U dorst: steek dan het *Dorst Bordje* in de lucht, onze topdiensters zullen dan je bestelling komen opnemen, zonder de andere ploegen te veel te storen.
- De toiletten bevinden zich daar. Roken kan buiten, maar gooi geen peukjes in de zandbak alstublieft, mijn kleuter vind dat ongetwijfeld lekker.

- Schrijf leesbaar, en dat geldt ook voor de leerkrachten
- Vermeld op elk antwoord blad de ploegnaam
- Saaie hulpmiddelen als een smartphone of tablet zijn uiteraard ten strengste verboden
- Bij namen verwachten we enkel de achternaam, als je de voornaam mee opschrijft en hij is fout, is het antwoord ook fout
- De jury heeft altijd gelijk, maar wordt met plezier op de vingers getikt  bij uitschuivers.
En de laatste regel:
- Have Fun, het is maar een kwis

Tot zover de disclaimer

## Rode draad

Tussen die vragen van vanavond zitten er een aantal die met mekaar in verband staan: een rode draad dus.
Elke vraag die verband houdt met die rode draad zal ik aangeven met dit: DING
Hoe sneller jullie dat verband raden, hoe meer punten dit oplevert:
Raden jullie het al na ronde 1: 10 punten, na ronde 2, 9 punten, enzovoort.
Dus zelfs als je bij ronde 8 nog een juist antwoord opschrijft, krijg je nog 3 punten.
Niet elke ronde bevat evenveel tips.
Onderaan elk antwoord blad zal je een vak vinden om jullie antwoord op te schrijven.
Let op: je mag ook maar een keer een antwoord opschrijven, dus misschien loont het om langer te wachten.

## Schifting vraag

Mochten er 2 ploegen evenveel punten halen, dan bepaalt een schiftings vraag de winnaar.
En die schiftings vraag luid als volgt:

*Hoe groot is het Kwis-team?*

En het Kwis team staat nu even recht om dat aanschouwelijk te maken:

Dus wat is de totale lengte van Stefan, Sara, Wannes, Annik en ik opgeteld, uitgedrukt in centimeter?
Dit antwoord kunnen jullie invullen op het antwoordblad van ronde 1.

## Pijama

Ik zien dat er een een aantal mensen grondig mijn mail van begin deze week gelezen hebben en in pijama naar hier gekomen zijn.
Vandaag was het immers Pijamadag op school om bednet in de kijker te zetten.
Dat ziet er allemaal gezellig uit, en het brengt ook al direct punten op.

En dat brengt ons gelijk naar en eerste tussen stand!
Kijk eens aan: direct aan de leiding:

## Beginnen

Er zijn vanavond 146 punten te verdienen, dus laten we er maar direct aan beginnen.


## Eerste tussenronde

Met een woordje uitleg over de eerste tussenronde

Bij het binnenkomen vonde jullie op jullie tafels een kleine versnapering. Hopelijk hebben jullie die nog niet opgegeten.
Want dat waren eigenlijk de vragen voor de eerste tussenronde.

De antwoord bladen daarvoor worden nu uitgedeeld. 
De vraag is simpel: hoe heetten de chips die jullie op tafel vonden?

De antwoorden worden opgehaald na ronde 2.


Klaar om er echt aan te beginnen?


# Ronde 1 - de kinderen stellen de vragen

We hebben weer een aantal kinderen opgetrommeld om voor ons vragen in te spreken.
Wij hebben er plezier aan gehad, de kinderen vonden het leuk, nu hopelijk jullie ook.

## 1  

We lieten de kinderen eerst een aantal foto's zien
We zoeken een gebouw

## 2 DING

Opnieuw een gebouw

## 3

We vroegen de kinderen ook een aantal begrippen uit te leggen.
Waar hebben ze het hier over?

## 4

Welke begrip leggen de kinderen hier met hand en tand uit?

## 5

Kinderen hebben ook hun eigen taal, ook al begrijpen ze die zelf soms niet.

## 6

Toen hebben we de koters een koptelefoon opgezet en ze gevraagd om de liedjes die ze hoorden voor ons te zingen.
We zoeken telkens titel en uitvoerder. En met uitvoerder bedoelen we dus niet de koters.

## 7

Titel en uitvoerder van het volgende liedje

## 8

De grotere kinderen kregen de tekst te zien samen met de muziek.

## 9

Zelfde vraag: titel en uitvoerder

## 10

En de laatste

# Ronde 2 - 2016

## 1

In 2016 is ons een enorme hoeveelheid rampspoed verkomen.
Een van de groten die gestorven is dat jaar is David Bowie, op 10 januari.

Hoe heette de laatste plaat die hij uitbracht, 2 dagen voor zijn dood?

## 2

Op 16 october, 19 dagen voor zijn dood, bracht ook deze man nog een meester werk uit:
Van wie is dit de laatste platenhoes?

## 3

Hoe heet het Brusselse metro station waar op 22 maart 2016 de terroristische aanslagen plaatsvonden?

## 4

We zullen proberen om het iets vrolijker te maken goed?

op 8 januari 2016 werd Joaquín Guzmán voor de derde keer opgepakt, Hij was het jaar ervoor voor de derde keer ontsnapt uit een Mexicaanse gevangenis. 
Hij werd dit jaar nog maar eens veroordeeld.

Wat is zijn bijnaam?

## 5

Op 15 juli vond in Turkije een poging tot staatsgreep plaats.
Bij welke groep in de samenleving legde president Erdogan hiervoor de schuld?

## 6

Op 8 November 2016 wordt een bepaald reality ster verkozen tot president.
Hoe heette de tv show waarin hij 11 seizoenen lang de hoofdrol mocht spelen?

## 7

Op 23 juni van dat jaar stemden de Britten om de EU te verlaten.
Hoe heette de politicus die de rol van hofnar speelde in dit drama?
Hij werd later nog een paar jaar "Secretary_of_State_for_Foreign_and_Commonwealth_Affairs", ofwel minister van buitenlandse zaken?

## 8

25 December 2016 is ons Georgios Kyriacos Panayiotou ontvallen, ook wel bekend als George Michael. Die was tussen 1981 en 1986 onderdeel van "Wham!". Hoe heette het andere onderdeel van "Wham!".  

## 9

Op 4 september 2016 wordt in Rome een non heilig verklaard door paus Fransicus.
Deze vrouw was tijdens haar leven en na haar dood een controversieel figuur, alhoewel ze door velen aanbeden werd om haar vele goede werken. Met welke stad wordt deze non altijd geassocieerd?

## 10

En om met een verderlichte noot af te sluiten 

Hoe heet deze deelnemer aan Temptation Island seizoen 2016?
Op cosmopolitan.com kan je - indien dat iets is dat je boeit - het volgende lezen over hem: 

Na Temptation Island heeft hij vooral slechte vrienden ontmoet en fouten gemaakt. "Ik gedroeg me alsof ik miljonair was en al mijn spaargeld ging op aan drank en feestjes. Het gerucht dat ik veel schulden heb klopt niet, maar ik heb er wel veel geld doorgejaagd."

"Ik kwam er op een gegeven moment achter dat het niet mijn echte vrienden waren en dat ze niet het beste met me voor hadden. 
Ze wilden gewoon meeliften op mijn bekendheid, maar toen de hype voorbij was, schoven ze me zo aan de kant. Ik voelde me erg misbruikt." Wat sneu voor puntje puntje puntje!

# Ophalen Tussen Ronde 1

# Uitdelen Tussen Ronde 2

# Ronde 3 - 3 maal 3

De 3 maal 3 maal 4 ronde is afgekeken van De slimste mens.

We tonen telkens 12 zaken en zoeken 3 verbanden tussen telkens 4 dingen.
Na 30 seconden verdwijnt helaas wel alles, je moet dus niet "pas" roepen of zo.

We tonen eerste een test vraag om duidelijk te maken hoe het in zijn werk gaat.

## test vraag
merk onderaan het balkje op dat aangeeft hoeveel tijd er nog rest, kwestie van minder stress te hebben en zo.

Duidelijk?

## 1

## 2

## 3

Hier moet wat uitleg bij: we tonen 12 bekende vlaamse acteurs.
We zoeken 3 televisie series waarin telkens 4 van die bekende koppen samen geacteerd hebben.

Nog eens?

We tonen dezelfde opdrachten nu, maar dan 20 seconden lang


# Ronde 4 - De decenniën
ofte wel was he tnu 70 80 90 2000 of 2010
of hoe dat programma op stubru tegenwoordig ook heet.

We zoeken telkens 1 decennium in deze ronde

Voor de duidelijkheid: als het antwoord 1990 is, zet je een kruisje in de kolom "90", want 1990 valt in de jaren "90".
1999 ook, 
2000 valt in het decennium dat we met "00" aangeduid hebben.

## 1

Wanneer kwam het legendarische computer spel DOOM uit?
Het was een van de eerste en gigantisch populaire First Person Shooters, draaide op MS-dOS en werd uitgebracht door id Software.

## 2

De reeks "You Rang, M'Lord?" liep 4 seizoenen op de BBC. In welk decennium werd de eerste aflevering van het eerste seizoen uitgezonden?
Er werd voordien ook nog een pilot uitgezonden, maar die interesseert ons niet.

## 3 DING

Marijn Devalk werd in 1992 snor van het jaar, en hij speelt onder andere Boma in FC De kampioenen.
In welk van de decenniën werd de eerste aflevering van F.C. De Kampioenen uitgezonden?

## 4 OOK DING

IN welk decennium moest Leona Detiege aftreden als burgemeester van Antwerpen naar aanleiding van de VISA affaire?

## 5

Ariel de kleine zeemeermin, de tekenfilm gebaseerd op het sprookje van Hans Christian Andersen, was de eerste animatiefilm waarmee Disney na lange tijd terug commercieel succes boekte.
In welk decennium kwam hij uit?

## 6

Een ander film die we zoeken. Iedereen weet dat Terminator 2 uitkwam in 1991,
maar in welk decennium kwam Terminator 3 uit?

## 7

We hebben een site gevonden waar het moment van eerste gebruik van bepaalde termen te vinden is: 
Merriam-Webster
op hun site staat , "America's most trusted online dictionary for English word definitions, meanings, and pronunciation." 
Een beetje de amerikaanse Van Dale dus.

De eerste term is "smartphone".
In welk decennium werd dit begrip voor het eerst gebruikt volgens Merriam-Webster.

## 8

Een andere term is "bucket list"

In welk decennium werd dit begrip voor het eerst gebruikt volgens Merriam-Webster.

## 9

Instagram valt niet meer weg te denken uit de leefwereld van hipsters en/of iedereen die tijd te veel heeft.

Maar in welk decennium werd deze App gelanceerd?

## 10

"U can't touch this"

simpel: in welk decennium werd dit nummer uitgebracht?


# Ophalen Tussen Ronde 2

# Pause


# Ronde 5 - Wat waren de vragen?

Bij deze ronde hoort ook een uitleg.

Mogen we jullie eerst vragen om allemaal recht te staan.
Je om te draaien, zodat je met je rug naar je tafel en je ploeg genoten staat
en vanaf nu niet meer te praten of te fluisteren.
Volledige stilte is belangrijk!

Ik ga nu 12 vragen stellen. Ik lees elke vraag 2 keer voor, in een random volgorde. Je hoort me dus in totaal 24 vragen stellen.
Als dat voorbij is, mogen jullie je terug omdraaien en de 12 antwoorden opschrijven. De volgorde van de antwoorden speelt uiteraard geen rol.

Duidelijk?
Hier gaan we:


- wat is de "wortel van 121"?
- wat is de eerste zin van "Ring Of Fire" van Johnny Cash?
- *PING* - Hoe heet de centrale horror figuur in Twin Peaks? 
- voor wat staat de afkorting FOMO?
- van wat is gsm de afkorting?
- wat is de hoofdkleur van de outfit van Olivia uit de kinderserie ROX?
- van wat is gsm de afkorting?
- Wie sprong op 11 juli 2001 van het Hilton Amsterdam Hotel?
- Wie sprong op 11 juli 2001 van het Hilton Amsterdam Hotel?
- wat is de hoogste berg van afrika?
- wat is de bijnaam van de nationale vrouwen basketploeg?
- welke letter staat er uiterst recht bovenaan een QWERTY toetsenbord?
- wat is de eerste zin van "Ring Of Fire" van Johnny Cash?
- wat is de hoofdkleur van de outfit van Olivia uit de kinderserie ROX?
- wat is de hoogste berg van afrika?
- *PING* Hoe heet de centrale horror figuur in Twin Peaks? 
- wat is de 16de letter van het alfabet?
- welke letter staat er uiterst recht bovenaan een QWERTY toetsenbord?
- wat is de "wortel van 121"?
- wat is de echte naam van Eddy Wally?
- wat is de bijnaam van de nationale vrouwen basketploeg?
- wat is de 16de letter van het alfabet?
- wat is de echte naam van Eddy Wally?
- voor wat staat de afkorting FOMO?


# ronde 6 - ABC

In deze ronde verwachten we 26 antwoorden.
Die antwoorden beginnen dus allemaal met een andere letter, zoveel is duidelijk.

## 1

We zoeken de nederlandse benamingen van de hoofdsteden van de provincies van België.
Let op: Als meerdere steden met dezelfde letter zouden beginnen, verwachten we wel _alle_ steden te zien bij die letter.
Dus onzinnig antwoord: als Oostende en Oostmalle provincie hoofdsteden zouden zijn, dan vul je bij O die 2 namen in.

## 2

De man links op de foto heeft _dochters_. Ik geef je alvast de namen van 2: *Billie* en *Dorien*, Hoe heten de anderen?
Ook bij deze vraag mogen jullie dus bij meerdere letters iets schrijven, als hij meerdere dochters heeft.

## 3

We zoeken 1 term die verwijst naar "Technologie, Wetenschap, Wiskunde en Bouwkunde" via een actieplan tracht de Vlaamse overheid de jeugd meer te interesseren voor wetenschap en techniek; het letterwoord wordt gebruikt in tal van samenstellingen.

## 4

De Star-Wars vraag!
Hoe heet dit personage uit Episode I – The Phantom Menace uit 1999. Gespeeld door Liam Neeson. Het is een Jedi Master en mentor van Obi-Wan Kenobi, gespeeld in die episode door Ewan McGregor.

## 5

We zoeken een amerikaans bedrijf dat een netwerk van satelieten in de lucht houdt voor wereldwijde voice en data communicatie met handheld telefoons en andere apparaten.
Met op dit moment 75 satelieten in de lucht is dit netwerk uniek omdat het op elke plek op aarde bereikbaar is.
De laatste reeks satelieten werd begin dit jaar nog door SpaceX van Elon Musk de ruimte ingeschoten.

Hoe heet dit bedrijf?

## 6

We zoeken het chromosoom dat in diploïde cellen van individuen van het ene geslacht in tweevoud voorkomt en in diploïde cellen van het andere geslacht in enkelvoud.

## 7

De chemie vraag: de Tabel van Mendeljev was recent nog in het nieuws want hij is net 150 jaar oud geworden.
Proficiat Tabel!
Wat is het symbool voor het chemisch element Fosfor?

## 8

We zoeken een Amerikaanse rock band in 1969 in Houston, Texas opgericht. De leden zijn Billy Gibbons, Dusty Hill, en drummer Frank Beard. Hun plaat Eliminator uit 1983 verkocht het best, met deze fantastische Ford op de cover.

## 9

Een televisiequiz en taalspel tussen een Vlaams en een Nederlands team, sinds 1989 uitgezonden door verschillende omroepen in Nederland, en de VRT. Sinds 2006 was het programma alleen nog bij de KRO te zien en sinds 2009 spelen zelfs alleen nog maar Nederlanders mee. Gepresenteerd door Marcel Van Tilt en Anita Witzier.

## 10

Op het internet wordt verschrikkelijk hard gescholden en slecht geargumenteerd. Een redenering die je vaak ziet terug komen is dat iemand een Nazi genoemd wordt omdat hij of zij een mening deelt met Hitler: "Hitler was tegen roken, jij bent tegen roken, dus jij bent een Nazi". Wat is de pseudo latijnse benaming van deze redenering? De term die we zoeken bestaan al lang voor het internet trouwens, al sinds 1953.

## 11

We zoeken de Amerikaanse versie van wat bij ons jeugdherberg heet.
Daar is de organisatie wel christelijk geïnspireerd en is het officieel een "oecumenisch-christelijke jongerenorganisatie"
"Het is er leuk toeven" zeggen ze er daar over.

## 12

Opgelet we zoeken de _voornaam_ van de volgende persoon.
Het spel is begonnen vorige zondag.
Weet u nog wie in het eerste seizoen 1998-1999 De Mol was?

## 13

We zoeken het Friese woord voor "vuilnisemmer"
De term werd ook gebruik als naam van een humoristisch tv programma dat 15 jaar liep op de nederlands tv, met onder andere Herman Koch, Michiel Romeyn en Kees Prins.

## 14

België heeft een opvolger gekozen voor de F-16 vliegtuigen.
Voor welk toestel is er gekozen?

jullie zullen wel vermoeden bij welke letter WE het antwoord verwachten, maar welk getal hoorde daar bij?

## 15

DING

Naar wie was het huidige Bevrijdingsdok genoemd?

## 16

We zoeken de naam van een Nederlands Film uit 1984.
Het verhaal gaat over de familie Vrijmoeth en speelt zich af in de jaren 30.  
Een heel dramatisch script met verschrikkelijke gebeurtenissen maken het 1 van de triestigste verhalen ever.

Bekende koppen waren Willeke van Ammelrooy en Herman Van Veen, die ook die verdomde soundtrack schreef.

## 17

Aardrijkskunde: op 1 januari 2019 annexeerde Aalter een kleinere buurgemeente.
De lokale machthebbers, waaronder Pieter De Crem, verbloemden de zaak nog met holle slogans als "samensterkerbeter" en "Totaalter" 
maar lachten ondertussen hun nieuwe onderdanen uit door hun straten te hernoemen:
de "Schoolstraat" werd bijvoorbeeld "de Pierlalaweg".
Hoe heet de gemeente die begin dit jaar fuseerde met Aalter?

## 18

Nog aardrijkskunde: We zoeken een Amerikaanse staat die bekend staat om zijn Salt Lakes en geweldige Monument Valley. 
De hoofdstad is Salt Lake City, en er is geen Beach.

## 19

Vorige vrijdag 8 maart was er een internationale feestdag. Wat werd over heel de wereld gevierd?


# Ronde 7 - Heeft U goed opgelet?

Deze ronde gaat over de kwis zelf, dus als u vanavond goed heeft opgelet, wordt dit een makkie.

## 1

Voor de pauze had ik een andere trui aan.
Welke kleur had die trui?

## 2

Een van de topdiensters, Heidi, heeft daarnet een ronde lang opgediend met iets op haar hoofd.
Wat was dat?

## 3

Daarnet tijdens de pauze heb ik allemaal nummers van 1 welbepaalde groep gespeeld.
Welke groep?

## 4

Om iets na 8 zijn we begonnen aan deze kwis.

Wat waren mijn eerste woorden van deze kwis?
Voor de opletters: ik was niet met mijn telefoon aan het spelen, maar ik heb ondertussen opgenomen wat ik zei als bewijs materiaal voor zo dadelijk.

## 5

DE jury hier naast mij zit al heel de avond naarstig te werken.
Ik heb ze in het begin van de kwis voorgesteld. Maar een persoon van diegenen die ik toen voorgesteld heb, zit er nu niet bij.

Wie missen we op dit moment aan de jury tafel?

## 6

Jullie weten wel nog wat het juiste antwoord was op de allereerste vraag van deze kwis,

DING
maar weten jullie nog wat het juiste antwoord was op de tweede vraag van de eerste ronde?

## 7

Buiten aan de inkomdeur hing een affiche voor het Schoolfeest. Op welke datum valt dat dit jaar?

## 8

Tijdens deze kwis waren er 3 vragen waarop het juiste antwoord hetzelfde was.
Was was het juiste antwoord op 3 vragen tot nu toe, dus met deze vraag erbij 4.

## 9

Bij de eerste ronde kregen jullie 10 filmpjes te zien met schattige kinderen die vanalles uitlegden en zongen.

Hoeveel kinderen deden er in totaal mee?

## 10

Welke ploeg stond op de eerste plaats toen we daarnet de laatste keer de tussenstand lieten zien?

# Ronde 8 - Lotto ronde

De antwoorden in deze ronde zijn allemaal cijfers.
Die cijfers mag u aankruisen op uw antwoord formulier, als vulde u de wekelijke lotto formulier in.

## 1

We beginnen met een wiskunde vraag: 
Hoeveel nullen zitten er in een triljoen?
Dus in de nederlandse woord trillioen, niet in het amerikaans "trillion"

## 2

Dit handgebaar in American Sign Language stelt een letter voor.
De hoeveelste letter in het alfabet stelt dit teken voor?

## 3

Dit is Max Verstappen, een deels Nederlands, deels Belgische autocoureur van 21 jaar oud.
Bij zijn debuut was hij de jongste autocoureur in het Formule 1 circuit.
Hij debuteerde in de Grote prijs van Australie in Melbourne, op 15 maart.
We willen weten in welk jaar van dit millennium hij debuteerde. in de Formule 1

## 4

Deze figuur is een Moebius ring, prachtig getekend door Escher.
Deze vraag gaat echter over een andere geometrische figuur:
En die vraag is

Hoeveel ribben heeft een balk?

## 5

Een voetbal vraag:
Op 3 maart viel het doek op Daknam. 
Lokeren degradeert naar tweede klasse. Anderlecht bezegelde het lot van de Oost-Vlamingen zonder het gaspedaal in te drukken. 
Santini en Gerkens brachten de bezoekers op een dubbele voorsprong. De aansluitingstreffer van Cevallos viel te laat. 

Na hoeveel jaar verlaat Lokeren eerste klasse?

## 6

Films worden tot op vandaag nog op pellicule gedraaid.
De stroken komen in verschillende afmetingen en maten.
Wat is de standaard breedte die het meeste gebruikt wordt, uitgedrukt in millimeter.
Leuk weetje: het digitale geluid dat bij de beelden hoort, wordt zowel links VAN, als tussen de gaatjes meegeleverd, in een soort QR code.

Wat is de standaard breedte van pellicule?

## 7

Nog een sport vraag, of toch licht aanleunend bij sport:
Hoeveel kegels staan er op een bowling baan?

## 8

"The Hitchhikers guide to the Galaxy" van Douglas Adams.
Hier is mijn beduimelde exemplaar te zien op mijn keuken tafel.
Geweldig grappig boek, zij het wat voor nerds.
De centrale grap van het boek draait om een super-computer die speciaal gebouwd werd om "The ultimate question of life, the universe and everything" op te lossen.

Met welk antwoord kwam die eerste super-computer af?
Nadien moest er dan een andere super-computer gebouwd worden om te weten te komen wat de vraag dan wel precies was.

MAar wat was dus het antwoord op "The ultimate question of life, the universe and everything"

## 9

Hoeveel drukwaterreactoren staan er in totaal volgens de website van de FANC in Belgische kerncentrales?

## 10

En de allerlaatste vraag van deze kwis:

DING

Hoeveel districten telt de stad antwerpen?

