# Lotto ronde

## 1

We beginnen emt een wiskunde vraag: 
Hoeveel nullen zitten er in een triljoen?
Dus in de nederlandse woord trillioen, niet in het amerikaans "trillion"

## 2

Dit handgebaar in American Sign Language stelt een letter voor.
De hoeveelste letter in het alfabet stelt dit teken voor?

## 3

Dit is Max Verstappen, een deels Nederlands, deels Belgische autocoureur van 21 jaar oud.
Bij zijn debuut was hij de jongste autocoureur in het Formule 1 circuit.
Hij debuteerde in de Grote prijs van Australie in Melbourne, op 15 maart.
We willen weten in welk jaar van dit millennium hij debuteerde.

## 4

Deze figuur is een Moebius ring, prachtig getekend door Escher.
Deze vraag gaat echter over een andere geometrische figuur:
En ie vraag is

Hoeveel ribben heeft een balk?

## 5

Een voetbal vraag:
Op 3 maart viel het doek op Daknam. 
Lokeren degradeert naar tweede klasse. Anderlecht bezegelde het lot van de Oost-Vlamingen zonder het gaspedaal in te drukken. 
Santini en Gerkens brachten de bezoekers op een dubbele voorsprong. De aansluitingstreffer van Cevallos viel te laat. 

Na hoeveel jaar verlaat Lokeren eerste klasse?

## 6

Films worden tot op vandaag nog op pellicule gedraaid.
De stroken komen in verschillende afmetingen en maten.
Wat is de standaard breedte die het meeste gebruikt wordt, uitgedrukt in millimeter.
Leuk weetje: het digitale geluid dat bij de beelden hoort, wordt zowel links VAN, als tussen de gaatjes meegeleverd, in een soort QR code.

Wat is de standaard breedte van pellicule?

## 7

Nog een sport vraag, of toch licht aanleunend bij sport:
Hoeveel kegels staan er op een bowling baan?

## 8

"The Hitchhikers guide to the Galaxy" van Douglas Adams.
Hier is mijn beduimelde exemplaar te zien op mijn keuken tafel.
Geweldig grappig boek, zij het wat voor nerds.
De centrale grap van het boek draait om een super-computer die speciaal gebouwd werd om "The ultimate question of life, the universe and everything" op te lossen.

Met welk antwoord kwam die eerste super-computer af?
Waarop alras een andere super-computer moest gebouwd worden om te weten te komen wat de vraag dan wel precies was.

MAar wat was dus het antwoord op "The ultimate question of life, the universe and everything"

## 9

Hoeveel drukwaterreactoren staan er in totaal volgens de website van de FANC in Belgische kerncentrales?

## 10

En de allerlaatste vraag van deze kwis:

DING

Hoeveel districten telt de stad antwerpen?

